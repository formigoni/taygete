#  vim: set syntax=dockerfile expandtab fdm=marker ts=2 sw=2 tw=100 et :

FROM archlinux

# Install requirements
RUN pacman --noconfirm -Sy
RUN pacman --noconfirm -S pacman-contrib
RUN curl -s "https://www.archlinux.org/mirrorlist/?country=US&country=DE&protocol=https&use_mirror_status=on" \
  | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 5 - > /etc/pacman.d/mirrorlist
RUN pacman --noconfirm -Syy
RUN pacman --noconfirm -S make cmake clang git python-pip
RUN pip install conan

# Build
RUN conan profile new default --detect
RUN conan profile update settings.compiler.libcxx=libstdc++11 default
RUN conan remote add pleiades https://api.bintray.com/conan/ruanformigoni/pleiades
RUN conan remote add fplus https://api.bintray.com/conan/ruanformigoni/functionalplus
RUN conan remote add epfl https://api.bintray.com/conan/ruanformigoni/epfl
ADD ./ /taygete/
WORKDIR /taygete/build
RUN conan install --build=missing ..
WORKDIR /taygete
RUN cmake -H. -Bbuild -D CMAKE_BUILD_TYPE=Debug -D CMAKE_CXX_COMPILER=g++
RUN cmake --build build

# Test
RUN ./build/test/test_graph -s
RUN ./build/test/test_graph_reader -s
