# Taygete - C++ 20 Data Structures

<table style="text-align: center">
  <tr>
    <td style="text-align: center" width="9999">
      <img  width="200px" src="./doc/character/taygete.png">
    </td>
  </tr>
</table>

---

[[_TOC_]]

## Documentation

The documentation of this library was made with [mdbook](https://rust-lang.github.io/mdBook/) and is available in this [link](https://formigoni.gitlab.io/taygete).

## Conventions

Taygete adheres to C++ container conventions, that includes naming and data member availability. E.g.:

### Members

| Member Type                          |
| -------------------------------------- |
| value_type                             |
| size_type                               |
| reference                               |
| const_reference                    |
| and so on... |

### Methods
| Member Functions Classes    |
| -------------------------------------- |
|  Element access                     |
| Iterators                                  |
| Capacity                                 |
| Modifiers                                |
|  and so on...                           |


## Data Structures

### Graph

---

Naming conventions are derived from the book:

> Jonathan L. Gross, Jay Yellen, and Ping Zhang. 2013. Handbook of Graph Theory, Second Edition (2nd. ed.). Chapman & Hall/CRC.

* A graph is denoted as $`G(V,E)`$.

* $`V`$ are the _vertices_.

* $`E`$ are the edges.

* $`Endpoints`$ are/is the extrema(s) of the edges.

* Vertices that share an edge are $`Adjacent`$

* Adjacent vertices are called $`Neighbors`$.

* A $`Proper\ Edge`$ is an edge that binds Two vertices.

* A $`Multi-Edge`$ is a collection of two or more edges with the same endpoints, e.g.:

  ```mermaid
  graph LR;
  	A-- a ---B;
  	A-- b ---B;
  ```
  
  Or
  
  ```mermaid
  graph LR;
  	A-- a -->B
  	A-- b -->B
  ```

* $`Simple\ Adjacency`$ between two vertices, occurs when they share exactly one edge.

* $`Edge-Multiplicity`$ of two vertices,  is the number of edges shared by them.

* A $`Self-Loop`$ is an edge that has the same vertex as endpoints.

  ```mermaid
  graph TD;
  	A---A;
  ```

* A directed edge has a $`tail`$ and a $`head`$, the $`head`$ is the arrowhead.

## Examples

### Graph

```cpp
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <taygete/graph/graph.hpp>
#include <taygete/graph/reader.hpp>

int main()
{
  namespace graph = taygete::graph;
  namespace reader = taygete::graph::reader;

  // Read input file
  std::fstream ifile{"circuits/c17.v"};

  // Check read result
  if( ! ifile.good() ) return EXIT_FAILURE;

  // Read the file into a buffer
  std::stringstream buffer; buffer << ifile.rdbuf();

  // Create a new graph
  graph::Graph<int32_t> g;

  // Create an insertion callback
  auto callback = [&g](auto&& edge){ g.emplace(edge); };

  // Use the file string as input to the file reader
  reader::Reader (buffer.str(), callback);

  // Print the graph info
  std::cout << "Vertices count: " << g.vertices_count() << std::endl;
  std::cout << "Edges count: " << g.edges_count() << std::endl;

  return EXIT_SUCCESS;
}
```

## Bugs?
<table style="text-align: center">
  <tr>
    <td style="text-align: center" width="9999">
      <img height="200px" src="./doc/character/taygete-shocked.png">
    </td>
  </tr>
</table>
Issues are most welcome! You can open one for bugs, feature requests, new data structures! Before the submission of pull requests, please follow the [Anubis](https://formigoni.gitlab.io/anubis/) C++ coding style guidelines. For more information, see [CONTRIBUTING.md](./CONTRIBUTING.md).