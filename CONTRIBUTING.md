# Contributing

[[_TOC_]]

## :whale: Tests with Dockerfile

To compile the docker image, you need to install `docker` and `docker-compose`:

Then in the project `root`, just run:

```cpp
docker-compose up tests
```

This command builds the project and runs the specified tests.

## :paperclip: Coding Guidelines

Please follow the coding guidelines specified [here](https://formigoni.gitlab.io/anubis/).

## :book: Test Creation

If you choose to novel create methods and/or data structures, please create the respective test modules.