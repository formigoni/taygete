// vim: set expandtab fdm=marker ts=2 sw=2 tw=100 et :
//
// @author      : Ruan E. Formigoni (ruanformigoni@gmail.com)
// @file        : graph-reader
// @created     : Thursday Jan 16, 2020 06:26:53 -03
//
// BSD 2-Clause License

// Copyright (c) 2020, Ruan Evangelista Formigoni
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// * Redistributions of source code must retain the above copyright notice, this
//   list of conditions and the following disclaimer.

// * Redistributions in binary form must reproduce the above copyright notice,
//   this list of conditions and the following disclaimer in the documentation
//   and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
// OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#pragma once

#include <optional>
#include <map>
#include <sstream>
#include <type_traits>
#include <lorina/lorina.hpp>
#include <taygete/concepts.hpp>

// taygete::graph::reader {{{
namespace taygete::graph::reader
{

// Namespaces {{{
using namespace taygete::concepts;
// }}}

// Concepts {{{
template<typename T>
concept Callback =
requires(T t)
{
  { t(std::make_pair(int32_t{},int32_t{}) ) } -> std::same_as<std::void_t<>>;
};
// }}}

// Enum Type {{{
enum class GateType
{
  AND,
  NAND,
  OR,
  NOR,
  XOR,
  XNOR,
  MAJ3,
};
// }}}

// class Reader {{{
template<Callback T>
class Reader : private lorina::verilog_reader
{
  private:
    mutable T callback;
    mutable int64_t id_counter;
    mutable std::map<std::string,int64_t> ids;
    mutable std::map<int64_t,GateType> gate_type;
  public:
  // Constructors {{{
    template<String S>
    Reader(S&& input, T callback);
  // }}}

  // Element Access {{{
    std::map<int64_t,GateType> const& data() const noexcept;
  // }}}

  // Private Methods {{{
  private:
    // Modifiers {{{
    virtual void on_module_header( const std::string& module_name,
      const std::vector<std::string>& inouts ) const noexcept override final;
    virtual void on_assign( const std::string& lhs,
        const std::pair<std::string, bool>& rhs ) const noexcept override final;
    virtual void on_and( const std::string& lhs,
      const std::pair<std::string, bool>& op1,
      const std::pair<std::string, bool>& op2 ) const noexcept override final;
    virtual void on_nand( const std::string& lhs,
      const std::pair<std::string, bool>& op1,
      const std::pair<std::string, bool>& op2 ) const noexcept override final;
    virtual void on_or( const std::string& lhs,
      const std::pair<std::string, bool>& op1,
      const std::pair<std::string, bool>& op2 ) const noexcept override final;
    virtual void on_nor( const std::string& lhs,
      const std::pair<std::string, bool>& op1,
      const std::pair<std::string, bool>& op2 ) const noexcept override final;
    virtual void on_xor( const std::string& lhs,
      const std::pair<std::string, bool>& op1,
      const std::pair<std::string, bool>& op2 ) const noexcept override final;
    virtual void on_xnor( const std::string& lhs,
      const std::pair<std::string, bool>& op1,
      const std::pair<std::string, bool>& op2 ) const noexcept override final;
    void update( const std::string& lhs,
      const std::pair<std::string, bool>& op1,
      const std::pair<std::string, bool>& op2,
      GateType const& type) const noexcept;
    void update( const std::string& lhs,
      const std::pair<std::string, bool>& rhs ) const noexcept;
    // }}}
  // }}}
};
// }}}

// Constructors {{{
template<Callback T>
template<String S>
Reader<T>::Reader(S&& input, T callback)
  : callback(callback)
  , id_counter(0)
{

  std::stringstream lorina_input; lorina_input << input;

  auto const op {lorina::read_verilog(lorina_input, *this)};

  if( !(op == lorina::return_code::success) )
  {
    std::cerr << "\033[91;1m * \033[mParsing error!" << std::endl;
  }
}
// }}}

// Element Access {{{
template<Callback T>
std::map<int64_t,GateType> const& Reader<T>::data() const noexcept
{
  return this->gate_type;
}
// }}}

// Modifiers {{{
template<Callback T>
void Reader<T>::on_module_header( const std::string& module_name,
    const std::vector<std::string>& inouts ) const noexcept
{
}

template<Callback T>
void Reader<T>::on_assign( const std::string& lhs,
    const std::pair<std::string, bool>& rhs ) const noexcept
{
  this->update(lhs, rhs);
}

template<Callback T>
void Reader<T>::on_and( const std::string& lhs,
  const std::pair<std::string, bool>& op1,
  const std::pair<std::string, bool>& op2 ) const noexcept
{
  this->update(lhs, op1, op2, GateType::AND);
}

template<Callback T>
void Reader<T>::on_nand( const std::string& lhs,
  const std::pair<std::string, bool>& op1,
  const std::pair<std::string, bool>& op2 ) const noexcept
{
  this->update(lhs, op1, op2, GateType::NAND);
}

template<Callback T>
void Reader<T>::on_or( const std::string& lhs,
  const std::pair<std::string, bool>& op1,
  const std::pair<std::string, bool>& op2 ) const noexcept
{
  this->update(lhs, op1, op2, GateType::OR);
}

template<Callback T>
void Reader<T>::on_nor( const std::string& lhs,
  const std::pair<std::string, bool>& op1,
  const std::pair<std::string, bool>& op2 ) const noexcept
{
  this->update(lhs, op1, op2, GateType::NOR);
}

template<Callback T>
void Reader<T>::on_xor( const std::string& lhs,
  const std::pair<std::string, bool>& op1,
  const std::pair<std::string, bool>& op2 ) const noexcept
{
  this->update(lhs, op1, op2, GateType::XOR);
}

template<Callback T>
void Reader<T>::on_xnor( const std::string& lhs,
  const std::pair<std::string, bool>& op1,
  const std::pair<std::string, bool>& op2 ) const noexcept
{
  this->update(lhs, op1, op2, GateType::XNOR);
}

template<Callback T>
void Reader<T>::update( const std::string& lhs,
  const std::pair<std::string, bool>& op1,
  const std::pair<std::string, bool>& op2,
  GateType const& type) const noexcept
{
  if( ! this->ids.contains(op1.first) ) this->ids[op1.first] = this->id_counter++;
  if( ! this->ids.contains(op2.first) ) this->ids[op2.first] = this->id_counter++;
  if( ! this->ids.contains(lhs) ) this->ids[lhs] = this->id_counter++;
  this->gate_type[id_counter] = type;
  this->callback(std::make_pair(this->ids[op1.first], this->ids[lhs]));
  this->callback(std::make_pair(this->ids[op2.first], this->ids[lhs]));
}

template<Callback T>
void Reader<T>::update( const std::string& lhs,
  const std::pair<std::string, bool>& rhs ) const noexcept
{
  if( ! this->ids.contains(lhs) ) this->ids[lhs] = this->id_counter++;
  if( ! this->ids.contains(rhs.first) ) this->ids[rhs.first] = this->id_counter++;
  this->callback(std::make_pair(this->ids[rhs.first], this->ids[lhs]));
}
// }}}

} // namespace taygete::graph::reader }}}
