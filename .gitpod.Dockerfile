FROM formigoni/alpine-gcc-10

RUN apk add --no-cache make cmake git py3-pip
RUN pip install -I conan

RUN conan profile new default --detect
RUN conan profile update settings.compiler.libcxx=libstdc++11 default
RUN conan remote add pleiades https://api.bintray.com/conan/ruanformigoni/pleiades
RUN conan remote add fplus https://api.bintray.com/conan/ruanformigoni/functionalplus
RUN conan remote add epfl https://api.bintray.com/conan/ruanformigoni/epfl

RUN apk add clang clang-dev
# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/
