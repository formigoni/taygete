# Reader

<!-- toc -->

This is a file reader module for the `graph` data structure.

## Reader class
The class has the following interface:
```cpp
template<Callback T>
class Reader : private lorina::verilog_reader;
```

Where `Callback` has to adhere to the following constraints:
```cpp
template<typename T>
concept Callback =
requires(T t)
{
  { t(std::make_pair(int32_t{},int32_t{}) ) } -> std::same_as<std::void_t<>>;
};
```

The callback is a `callable` which receives an edge as input, and performs an insertion operation on
the graph.

## Constructors
```cpp
template<typename Str>
  requires cp::ConvertibleTo<Str,std::string>
Reader(Str&& input, T callback);
```

## Formats
The currently supported formats are:
* Verilog

## Examples

### Verilog

#### Read a file

#### Read a string
